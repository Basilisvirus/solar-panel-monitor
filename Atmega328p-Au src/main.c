/*
 * lm1085.c
 *
 * Created: 9/21/2020 6:14:55 AM
 * Author : Christianidis Vasileios
 */ 
#include <stdio.h>
#include <stdlib.h>

#define F_CPU 16000000UL //the clock of the cpu (internal is 8 000 000 hz)
//Asynchronous double speed mode
//#define BRC ((F_CPU/(16*38400))-1) //define the baud rate (usually 9600)

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <util/delay.h>

#define BIT_GET(p,m) ((p) & (m)) //if(BIT_GET(foo, BIT(3)))
#define BIT_SET(p,m) ((p) |= (m)) //BIT_SET(foo, 0x01); BIT_SET(foo, BIT(5));
#define BIT_CLEAR(p,m) ((p) &= ~(m)) //BIT_CLEAR(foo, 0x40);
#define BIT_FLIP(p,m) ((p) ^= (m)) //BIT_FLIP(foo, BIT(0));
//BIT_WRITE(BIT_GET(foo, BIT(4)), bar, BIT(0)); (To set or clear a BIT based on BIT number 4:)
#define BIT_WRITE(c,p,m) (c ? BIT_SET(p,m) : BIT_CLEAR(p,m)) 
#define BIT(x) (0x01 << (x)) // 0<x<7
#define LONGBIT(x) ((unsigned long)0x00000001 << (x))

#define SECINTERVAL 15 //The interval in seconds where the timer2 counter counts.



//TX functions
void sw(const char toWrite[]); //print a string on serial
void swn(uint64_t num, int type, int ln); //print a Register or a number (no float), in hex(2) or in decimal(10)
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it); //Take a float and print it on serial

//Serial Write Hex
//Use like: swh("2A",0,0); to send a char
//Or like: swh(" ",0x2A,1); to send a Hex
void swh(const char toWriteChar[], uint8_t toWriteInt, uint8_t useInt);

//RX functions
void readUntill(char c_rx, uint8_t pause);


//======================TX START======================
volatile uint8_t len, k=0; //unsigned 8 bit integer
volatile char str[40];
char str_floatx[40];
//======================TX END=======================

//======================RX START=======================
char rxBuffer[256];
uint8_t rxBufferuint[256];
volatile char udr0 = '\0', rx_stop='\0'; /*  NULL = \0  */
volatile uint8_t rxReadPos = 0, rxWritePos = 0, readString =0, rxComplete=0;
uint8_t pauseReadingRx;
uint8_t rxProcessing = 0; //which processing array block are we
volatile uint8_t rxBufferuintFlush = 0;

/*
rxComplete is set when a receive is complete.
*/
//======================RX END=======================

//===================TWI functions=============
/*
***TWI_Init accepts frequency of the TWI in Hz, and automatically calculates the values for the registers/prescalers.
***TWI_Start(), TWI_Read() and TWI_Write() may accept a status code to check the response of the TWI communication line.
The return value may be:
0 -> Command applied sucessfully (if no status code to check is given, aka code = 0x00) but it does not check the reply.
1 -> Timer to apply command exceeded ERROR (status code to check was given). Is TWI pulled up with external resistors?
2 -> Sucess response from TWI (status code to check matches the response)
Anything else -> ERROR:TWI response that is different from the status code given for check.
*/
//waits until the given status code appears
uint8_t TWI_Check_Status_Code(uint8_t statusCodeToCheck);
//inits TWI with a bit rate.
void TWI_Init(uint32_t desired_freq, uint8_t slaveAddr);
//Starts the twi, sends start signal.
uint8_t TWI_Start(uint8_t CheckStartStatus);
//sends data to twi
uint8_t TWI_Write(uint8_t Data, uint8_t CheckWriteStatus);
//from the twi. Read result is in TWDR.
uint8_t TWI_Read(uint8_t CheckReadStatus);
//Stop the TWI
void TWI_Stop();
//print the response as dialog
void TWI_print_status(const char message[], uint8_t checkResponse);

volatile uint8_t twiTimerStart = 0; //for init_timers()
//================TWI FINISHED===============


//functions
//initializes the mcu, the first code that runs on main
uint8_t processMessage(); 
void init_rxtx_function(); //initializes only rxtx functionality
void init_timers();//used for twiTimerStart

//VARS
char user_input = '/0';
uint8_t errorByte = 0x00;
//flag: if the device is awake.
uint8_t isAwake = 0x00;
//Get device status array
volatile uint8_t devStatus[40];
volatile uint8_t randHelper;


//wait for address reference
void TWI_Match_ACK(uint8_t DataToSend)
{
	//while Own SLA+W has NOT been received; and ACK has NOT been returned
	while( ((TWSR & (0xF8))!= 0x60) && ((TWSR & (0xF8))!= 0xA8) )
	{
		//TWEA:enable the ackn. TWEN:enable TWI. TWINT:execute command
		TWCR = (1<<TWINT)|(1 <<TWEN)|(1 << TWEA);
	/*(1 << TWINT) makes TWINT 0. We want the above line to check for incoming address non stop*/
		/* when command is done executed, TWINT will be 1*/
		//WHile TWINT =0 wait (while command is being executed wait)
		while((TWCR & (0B10000000)) == 0);
		
		if((TWSR & (0xF8))== 0xA8)
		{
			TWI_Write(DataToSend,0);
			sw("Data written \n\r");
			break;
		}
		else
		{
			TWI_Read(0);
			sw("Data read: ");
			swn(TWDR,16,1);
			sw("\r");
		}
	}
}



int main(void)
{	
	init_timers();
	init_rxtx_function();
	readUntill('\0', 0);//start the RX buffer	
	
	sw("init twi\n\r");
	TWI_Init(100000, 0x17);//0001011 1 (0x0B + 1)
	
	while(1)
	{
		srand(udr0 + twiTimerStart + randHelper);
		uint8_t randomVal = rand();
		sw("sending ");
		swn(randomVal,10,1);
		TWI_Match_ACK(randomVal);//twi wire interface
		sw("done\n");
	}
	
	
	
	/*
    while (1) 
	{		
		static uint8_t eepValInLoop; //eeprom read value (in loop)
		
		if(rxBufferuint[rxProcessing] == 0x23)
		{
			uint8_t msgReturn = processMessage();
			udr0 = '\0';
		}
		ringBufferShift();//shift to read the next character
	}//end while
	*/
	
}

//this goes in while(1) as a loop, repeats itself
uint8_t processMessage()
{	
	//initialize user input
	static uint8_t inputProc = 0xFF;
	//the Command tag to be send back as a response at the end of the processing.
	static uint8_t responsebyte = 0x00;	
	//reset all error bits. use '1' to not reset an error bit
	errorByte = errorByte & 0B00000000;

	static uint32_t commErrorLoop=0;
	
	//============COMMAND TAG============
	//find the distance between the buffer counters
	static uint8_t saverxProcessing;
	saverxProcessing = rxProcessing;
	do //and use ring buffer here aka know when to increase the rxProcessing
	{
		ringBufferShift();
		/*commErrorLoop++;
		if(commErrorLoop == 1000000)
		{
			sw("slow communication\n");	
		}*/
		
	}while( saverxProcessing == rxProcessing );//while buffer is empty
	//the buffer is not empty, read the data
	inputProc = rxBufferuint[rxProcessing];//The command tag is saved here.
	
	
	//============NUM BYTES============
	saverxProcessing = rxProcessing;
	do //and use ring buffer here aka know when to increase the rxProcessing
	{
		ringBufferShift();
	}while( saverxProcessing == rxProcessing );//while buffer is empty
	
	//the buffer is not empty, read the data
	uint8_t followingBytesNum = rxBufferuint[rxProcessing];//the number of following data are stored here.

	
	//============DATA============
	//set a size to read input data
	uint8_t* dataInputArray = (uint8_t*) malloc(followingBytesNum * sizeof(uint8_t));
	//free(dataInputArray); to free the array when no longer needed
	if(followingBytesNum>0)//if we have following bytes
	{//make an array of that size

		static uint8_t tempIloop;//loop to fill the array
		
		for(tempIloop=0; tempIloop<followingBytesNum; tempIloop++)
		{
			saverxProcessing = rxProcessing;
			do //and use ring buffer here aka know when to increase the rxProcessing
			{
				ringBufferShift();
			}while( saverxProcessing == rxProcessing );//while buffer is empty
			
			dataInputArray[tempIloop] = rxBufferuint[rxProcessing];//save the incoming data
		}//end for()
	}//end if(followingBytesNum>0)
	
	
	//============END TAG============
	saverxProcessing = rxProcessing;
	do //and use ring buffer here aka know when to increase the rxProcessing
	{
		ringBufferShift();
	}while( saverxProcessing == rxProcessing );//while buffer is empty
	
	//now, end tag should be received
	if(rxBufferuint[rxProcessing] == 0x0A)//if end tag detected
	{
		BIT_CLEAR(errorByte, BIT(3));//End tag error is not error cause
	}
	else//if there is no end tag
	{
		BIT_SET(errorByte, BIT(3));//End tag error
	}

	//same Command tag will be used, so copy the command
	responsebyte = inputProc;
	//This message will be a response to a prev message
	BIT_SET(responsebyte, BIT(7));

	//Response check
	static uint8_t bitHelper;
	
	if(BIT_GET(inputProc, BIT(7)))//if the incoming message is a response
	{
		bitHelper = 1;
	}
	else//if the incoming message is not a response
	{
		bitHelper = 0;
	}

	//Quick bit
	if(BIT_GET(inputProc, BIT(6)))
	{
		bitHelper = 1; 
	}
	else
	{
		bitHelper = 0;
	}

	//Check command type
	bitHelper = (inputProc & 0B00111110);//AND mask
	bitHelper = bitHelper >> 1;//shift bits one position right. (Bit helper now is 000XXXXX)


	if
	(//if commands that need following data, have no following data
	( (bitHelper == 0B00000000) && (followingBytesNum == 0) )
	||//OR for all other commands that do not need following data, we have following data
	( 0 )
	)
	{
		BIT_SET(errorByte, BIT(4));//Set Following bytes error
	}
	else
	{
		BIT_CLEAR(errorByte, BIT(4));//Following bytes is not error cause
	}
	
	free(dataInputArray);//free the input data array
		
	return 0;
}//end of processMessage


//overflow for keeping track of time
ISR  (TIMER2_OVF_vect)
{
	randHelper++;
	//static uint8_t t2ovf = 0;
	
	if(twiTimerStart)//if we want to start its timer
	{
		twiTimerStart++;//increase it
		
		if(twiTimerStart >= 100)//if it reaches 20
		{
			twiTimerStart = 0;//make it zero again
		}
	}
}


void init_rxtx_function()
{
	//==================================TX START====================================
	UBRR0H = 0;// (BRC >> 8); //Put BRC to UBRR0H and move it right 8 BITs.
	UBRR0L = 0;//BRC;
	BIT_SET(UCSR0A, BIT(1));//U2Xn = 1//double speed
	UCSR0B = (1 << TXEN0); //Trans enable
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); //8 BIT data frame

	//ENABLE interrupts
	sei();
	//==================================TX END=====================================
	//================================== RX START===========================================
	UCSR0B |= (1 << RXEN0) | (1 << RXCIE0); //RX enable and Receive complete interrupt enable
	//================================== RX END=============================================
}

void init_timers()
{
	// /Unknown prescaler
	BIT_SET(TCCR2B, BIT(2));//CS22=1
	BIT_SET(TCCR2B, BIT(1));//CS21=1
	BIT_SET(TCCR2B, BIT(0));//CS20=1
	
	//MODE NORMAL
	BIT_CLEAR(TCCR2B, BIT(3));//WGM22=0
	BIT_CLEAR(TCCR2A, BIT(1));//WGM21=0
	BIT_CLEAR(TCCR2A, BIT(0));//WGM20=0
	
	//PINS DISCONNECTED, NORMAL OPERATION.
	BIT_CLEAR(TCCR2A, BIT(7));//COM2A1=0
	BIT_CLEAR(TCCR2A, BIT(6));//COM2A0=0
	BIT_CLEAR(TCCR2A, BIT(5));//COM2B1=0

	BIT_CLEAR(TCCR2A, BIT(4));//COM2B0=0
	
	//ENABLE SPECIFIC INTERRUPTS
	BIT_SET(TIMSK2, BIT(0));//TOIE2=1 (OVERFLOW INTERRUPT)
}



//==================STARTED TWI====================

/*
prints dialog explaining the return/checked response code of the TWI.
=0 Command applied successfully, no code to check was given.
=1 ERROR: timer to apply command exceeded. Do we use pull-up resistors on TWI line?
=2 success reply from TWI, command as expected.
else: 
TWI response not what expected. Got: ...
*/
void TWI_print_status(const char message[], uint8_t checkResponse)
{
	sw(message);
	
	if(checkResponse == 0)
	{
		sw(" =0 Command applied successfully, no code to check was given. \n");
	}
	else if(checkResponse == 1)
	{
		sw(" =1 ERROR: timer to apply command exceeded. Do we use pull-up resistors on TWI line? \n");
	}
	else if(checkResponse == 2)
	{
		sw(" =2 success reply from TWI, command as expected. \n");
	}
	else
	{
		sw("TWI response not what expected. Got: ");
		swn(checkResponse,16,1);
	}
}

//waits until the given status code appears
uint8_t TWI_Check_Status_Code(uint8_t statusCodeToCheck)
{
	if( ((TWSR&(0XF8)) != statusCodeToCheck))
	{
		sw("ERR Given status code was not returned. Instead we got: ");
		swn(TWSR&(0XF8),16,1);
		sw("\r");
		return (TWSR&(0XF8));//return the TWI error code (NACK)
	}
}


//inits TWI with a bit rate automatically
void TWI_Init(uint32_t desired_freq, uint8_t slaveAddr) //desired freq in Hz
{
	
	if(slaveAddr)//if slave address is non-zero, set up as slave.
	{
		TWAR = slaveAddr;//init slave address
		// If the LSB in TWAR is set, the TWI will respond to the general call address (0x00),
		// otherwise it will ignore the general call address
		BIT_SET(TWCR, BIT(2));//TWEN=1 Twi enabled
		BIT_SET(TWCR, BIT(6));//TWEA=1 Ack of slave enabled
		BIT_CLEAR(TWCR, BIT(5));//TWSTA=0
		BIT_CLEAR(TWCR, BIT(4));//TWSTO=0
	}
	else
	{
		uint32_t twbr_result;//calculated twdr for given prescaler
		uint16_t prescaler=1;//initial prescaler

		do
		{
			twbr_result = (F_CPU - (16*desired_freq) );
	
			twbr_result = twbr_result / ( 2 * prescaler * desired_freq);//function to find twbr
	
			prescaler = prescaler*4;//next prescaler

		//prescaler cant be >64, twbr cant be > 255
		} while ( (twbr_result > 255) && (prescaler/4 < 64) );

		prescaler = prescaler/4;//restore the last prescaler


	//if twbr failed or prescaler is >64
	if( (twbr_result > 255) || (prescaler!=1 && prescaler!=4 && prescaler!=16 && prescaler!=64) )
	{
		sw("TWI init failed\n");
	}
	else//function succeeded
	{
		TWBR = twbr_result;//save twbr
	
		//save prescaler
		if(prescaler == 1)
		{
			BIT_CLEAR(TWSR,BIT(1)); //TWPS1=0;
			BIT_CLEAR(TWSR,BIT(0)); //TWPS0=0;
		}
		else if (prescaler == 4)
		{
			BIT_CLEAR(TWSR,BIT(1)); //TWPS1=0;
			BIT_SET(TWSR,BIT(0)); //TWPS0=1;
		}
		else if (prescaler == 16)
		{
			BIT_SET(TWSR,BIT(1)); //TWPS1=1;
			BIT_CLEAR(TWSR,BIT(0)); //TWPS0=0;
		}
		else if (prescaler == 64)
		{
			BIT_SET(TWSR,BIT(1)); //TWPS1=1;
			BIT_SET(TWSR,BIT(0)); //TWPS0=1;
		}
	
		sw("twbr is: ");
		swn(TWBR,10,1);
	
		sw("prescaler is: ");
		swn(prescaler,10,1);
		}
	}	
}



//Sends a start signal to TWI
uint8_t TWI_Start(uint8_t CheckStartStatus)
{
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTA);// (1 << TWINT) makes twint 0
	/*Immediately after this operation, TWINT is set to 0, a Start is issued
	 until the transfer is done. When transfer is done, TWINT becomes 1*/
	twiTimerStart=1;//start counting
	//while TWINT =0 AND while timer has not ended, do nothing. the TWINT will be 1 when start is done.
	while((TWCR&(0B10000000))==0  && (twiTimerStart != 0) );
	
	if(twiTimerStart)//timer to apply command is >0
	{
		twiTimerStart = 0;//reset the timer
	}
	else//TWI ended with time exceeded error.
	{
		return 1;//timer to apply command exceeded
	}

	if(CheckStartStatus)//if we want to check for a status
	{
		return TWI_Check_Status_Code(CheckStartStatus);//returns the status
	}
	else
	{
		return 0;//no error
	}
}

//sends data to TWI
uint8_t TWI_Write(uint8_t Data, uint8_t CheckWriteStatus)
{
	TWDR = Data; //data to be sent
	TWCR = (1 << TWINT) | (1 << TWEN); //Execute command (TWINT), enable TWI (TWEN).
	twiTimerStart = 1;
	//while TWINT =0 AND while timer has not ended, do nothing. the TWINT will be 1 when start is done.
	while((TWCR&(0B10000000))==0  && (twiTimerStart != 0) );
	
	if(twiTimerStart)//timer to apply command is >0
	{
		twiTimerStart = 0;//reset the timer
	}
	else//TWI ended with time exceeded error.
	{
		sw("time exceeded\n\r");
		return 1;//timer to apply command exceeded
	}

	if(CheckWriteStatus)//if we want to check for a status
	{
		return TWI_Check_Status_Code(CheckWriteStatus);//returns the status
	}
	else
	{
		return 0;//no error
	}
	
}

//Read from the TWI, Read result is in TWDR.
uint8_t TWI_Read(uint8_t CheckReadStatus)
{
	//Enable TWINT(make it 0) to execute command, Data byte will be received and ACK will be returned
	TWCR = (1 << TWINT) | (1 << TWEA)| (1 << TWEN);
	//waiting for all data to be read
	twiTimerStart = 1;
	//while TWINT =0 AND while timer has not ended, do nothing. the TWINT will be 1 when start is done.
	while((TWCR&(0B10000000))==0  && (twiTimerStart != 0) );
	
	if(twiTimerStart)//timer to apply command is >0
	{
		twiTimerStart = 0;//reset the timer
	}
	else//TWI ended with time exceeded error.
	{
		return 1;//timer to apply command exceeded
	}

	if(CheckReadStatus)//if we want to check for a status
	{
		return TWI_Check_Status_Code(CheckReadStatus);//returns the status
	}
	else
	{
		return 0;//no error
	}
}

//Stops the TWI
void TWI_Stop()
{
	TWCR = (1 << TWINT)|(1 << TWSTO); //TWI stop and TWI interrupt enable to execute command
}

//initiates Pinouts
void init_GPIO()
{
	
}
//==================FINISHED TWI====================

void sw(const char toWrite[]){
	len = strlen(toWrite); //take size of characters to be printed
	k=0;	//initialize k
	//UDR0=0; //make sure UDR0 is 0
	
	while (k<len) //while i is less than the total length of the characters to be printed
	{
		if (  ((UCSR0A & 0B00100000) == 0B00100000)  ){ //if UDRE0 is 1 (aka UDR0 is ready to send)
			UDR0 = toWrite[k]; //put the next character to be sent (now, UDRE0 is 0)
			k++;		//increase the position, and wait until UDRE0 is 1 again
		}
	}
	//udr0 = '\0';
}

//Serial Write Hex
//Use like: swh("2A",0,0); to send a char
//Or like: swh(" ",0x2A,1); to send a Hex
void swh(const char toWriteChar[], uint8_t toWriteInt, uint8_t useInt)
{
	
	while (  ((UCSR0A & 0B00100000) != 0B00100000)  )//if UDRE0 is 1 (aka UDR0 is ready to send)
	{
		//Wait
	}

	//if we want to send a uint_8t
	if(useInt)
	{
		UDR0 = toWriteInt;
	}
	else
	{
		UDR0 = strtol(toWriteChar, 0, 16); //Transform char to hex and put the next character to be sent
	}
}



void swn(uint64_t num, int type, int ln) //take an int and print it on serial
{
	static char str_intx[50];//declare a string of 50 characters
	
	ltoa(num, str_intx, type);//convert from long to char and save it on str
	
	sw(str_intx); //serial write str
	
	if(ln == 1) //if we want a new line,
	{
		sw("\n");
	}
	
}

/*
Input: float number, output: print float number to serial, OR only return the float as string.
*/
void swf(double numf, uint8_t lnf, uint8_t extra_accur, uint8_t print_it) //Take a float and print it on serial
{
	/*
	Those do not work well
	*/
	/*if(extra_accur == 11)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 1000)));
	}
	else if(extra_accur == 22)
	{
		sprintf(str_floatx,"%d.%02u", (int) numf, (int) fabs(((numf - (int) numf ) * 100))); //edited to accept negative numbers
		//sprintf(str_floatx,"%d.%02u", (int) numf, (int) ((numf - (int) numf ) * 100) ); //this was te original function
	}*/
	
	if(extra_accur == 0 || extra_accur == 1)//works with very good accuracy
	{
		/*
		Needs library linking. Below tutorial is for Microchip studio.
		Project properties toolchain..XC8 Linker/General -> Tick 'Use vprintf library(-Wl,-u,vprintf)
		And: XC8 Linker/Miscallaneous insert at 'Other linker flags' this: -lprintf_flt
		https://startingelectronics.org/articles/atmel-AVR-8-bit/print-float-atmel-studio-7/
		*/
		sprintf(str_floatx, "%f", numf); //needs library linking or itll print something like ' ?); '
	}

	if(print_it)
	{
		sw(str_floatx);	
		
		if(lnf == 1) //if we want a new line,
		{
			sw("\n");
		}
	}
}


//==================================TX END=====================================

/*
the result (input) is stored in: char rxBuffer[]
*/
void readUntill(char c_rx, uint8_t pause)
{
	rxBuffer[0] = '\0';
	rxWritePos = 0;//begin writing in the buffer from pos 0
	readString = 1;//interrupt will read and save strings
	rx_stop = c_rx; //interrupt will use the global var rx_stop to stop reading the string
	pauseReadingRx = pause;//pass the pause option to the global variable to be used in RX vextor
	
	if(pause)//if we want the code to be paused here
	{
		do
		{//wait until the input has been received
			
		}while(readString == 1);
	}

}


ISR(USART_RX_vect)
{
	if(readString == 1)//if we are using readUntill
	{		
		rxBufferuint[rxWritePos] = UDR0;//save incoming char
		
		/*
		rxBuffer[rxWritePos] = UDR0;//save incoming char
		rxBuffer[rxWritePos+1] = '\0' ;//end char
		
		
		if( (rxBuffer[rxWritePos] == rx_stop) && (pauseReadingRx == 1) )
		{//if we meet the end char AND the Pause reading Rx is active
			readString = 0;
			//when you initialize a character array by listing all of its characters 
			//separately then you must supply the '\0' character explicitly
			rxBuffer[rxWritePos] = '\0' ;
		}
		*/
		
		rxWritePos++;
		
		if(rxWritePos == 0)
		{//if we are back at 0
			rxBufferuintFlush = 1;//buffer flushed and overflowed
		}
		
		rxComplete = 0B00000001;
	}
	else
	{
		udr0 = UDR0;	//udr0 variable can be used to store the input from the user.
		rxComplete = 0B00000001;//flag for receive complete.
	}
}

//==================================RX END==================================


/*
Shifts the buffer pointer to the next array block, if there is any available next block. This function does not pause the
Code. If we are waiting for an incoming package to arrive, we should run this function inside a loop, by storing the rxProcessing
value to another variable before the loop, and wait untill this variable changes. This will mean that the buffer moved aka
a new package arrived.
*/
void ringBufferShift()
{
	if( (rxProcessing < (rxWritePos-(1))) || ( (rxBufferuintFlush == 1U) && ( (uint8_t)(rxProcessing + 1U) != ((uint8_t)rxWritePos) ) ) )
	{//load the next block of the buffer
		
		++rxProcessing;
		
		if(rxProcessing == 0)
		{//if we are back from where we started
			rxBufferuintFlush = 0;//reset the flush flag
		}
	}
}
