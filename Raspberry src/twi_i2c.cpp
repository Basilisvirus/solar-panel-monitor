
//Run with:
//g++ twi_i2c.cpp -l wiringPi -lcurl -o twi_i2c && ./twi_i2c

#include <iostream>
#include <errno.h>
#include <wiringPiI2C.h>
#include <stdio.h>
#include <unistd.h>//for sleep
#include <curl/curl.h>//for curl (needed to sudo apt-get install libcurl4-openssl-dev)
#include <string.h>

using namespace std;

int main()
{
   cout << "Started" << endl;
   int fd, result;

   // Initialize the interface by giving it an external device ID.
   // The MCP4725 defaults to address 0x60.   
   //
   // It returns a standard file descriptor.
   // 
   fd = wiringPiI2CSetup(0x0b);

   cout << "Init result: "<< fd << endl;
   while(1)
   {
      result = wiringPiI2CRead(fd);

      cout << "result: " << result <<endl;
      
      char str_intx[2];//Where to save the output result (the string)
      snprintf(str_intx, sizeof(str_intx), "%d", result);
      cout << str_intx[0] << endl;
      
      char command[] = "curl -X POST -F 'voltage=40.2' https://piday.gr/raspberry/post.php | grep 'Voltage:'";
      command[25]=str_intx[0];
      command[26]='5';
      command[28]='3';
      system(command);
      sleep(5);
   }
}

