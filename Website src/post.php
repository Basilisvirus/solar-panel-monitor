<!DOCTYPE html>
<html>
<head>
  
<?php
    //Check if the request is POST
    if (isset($_POST['voltage'])) {
      
        // Grab values from POST within this file
        $voltage = $_POST['voltage'] ?? '';
        
        // Overwrite the POST value to a file
        $file = "voltage.txt"; 
		$Saved_File = fopen($file, 'w'); 
		fwrite($Saved_File, $voltage); 
		fclose($Saved_File);      
    }
?>  
 
 <script
         src="https://code.jquery.com/jquery-1.12.4.min.js"
         integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
         crossorigin="anonymous">
 </script>
  
 <title>Voltage monitoring v1.0</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>        
        * {
            box-sizing: border-box;
        }
        .column {
            float: left;
            width: 33.33%;
            padding: 1px;
        }
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        table, th, td {
            border: 3px solid white;
        }
        body {
            background-color: #00281f;
            color: white;
        }
    </style>
  
</head>
    <body>    
        <div class="value">
        </div>
        <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <dl>
                <dt>Input voltage value manualy for testing purposes and press 'send manual request' button </dt>
                <dd><input type="text" name="voltage" value="" /></dd>
                <dt><br>OR send a http request from anywhere: curl -X POST -F 'voltage=14.0'  https://piday.gr | grep 'Voltage:'</dt>
            </dl>    
            <div id="operations">
                <input type="submit" value="send manual request" />
            </div>
        </form>
    </body> 
        <script>              
          function get_fb(){
          var feedback = $.ajax({
              type: "POST",
              url: "/raspberry/ajax.php",
              async: false
          }).success(function(){
              setTimeout(function(){get_fb();}, 5000);
          }).responseText;

          $('div.value').html(feedback);
          }
          get_fb();   
      </script>
</html>

